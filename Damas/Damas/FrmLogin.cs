﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
using Servicios;

namespace Damas
{
    public partial class FrmLogin : Form
    {
        public Juego juego;
        public Frmtablero frmtablero;
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (juego.Login(Sesion.Encriptar(textBox1.Text), Sesion.Encriptar(textBox2.Text))== 1)
            {
                juego.Tablero.Fichas_a_Tablero(juego.Partida.Jugadores);
                frmtablero.Cargar_Fichas_Jugador(juego.Partida.Jugadores);
            }
            else
            {
                MessageBox.Show("Verifique Usuario y Contraseña");
            }
            Close();
        }
    }
}
