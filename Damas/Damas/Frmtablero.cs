﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Damas
{
    public partial class Frmtablero : Form
    {
        public Juego juego;
        private Image roja = Image.FromFile(@"D:\Documentos\Source\Repos\Las_Damas\Damas\Damas\Resources\ficha_roja.png");
        private Image azul = Image.FromFile(@"D:\Documentos\Source\Repos\Las_Damas\Damas\Damas\Resources\ficha_azul.png");
        private Image azul_reina = Image.FromFile(@"C:\Users\Pimpon\Source\Repos\las_damas\Damas\Damas\Resources\ficha_rey_azul.png");
        private Image roja_reina = Image.FromFile(@"C:\Users\Pimpon\Source\Repos\las_damas\Damas\Damas\Resources\ficha_rey_roja.png");
        private PictureBox box;
        private PictureBox boxactual;
        private PictureBox boxanterior;
        private int a = 50;
        private Localizacion ubiactual = new Localizacion();
        private Localizacion ubianterior = new Localizacion();
        private Size size = new Size(50, 50);
        private Casilla seleccionado;
        private Casilla a_mover;
        private PictureBox boxsel;
        private PictureBox boxamov;

        public Frmtablero()
        {
            InitializeComponent();
        }
        public void CargaTablero()
        {
            this.Controls.Clear();
            for (int fila = 1; fila < juego.Tablero.Filas; fila++)
            {
                for (int columna = 1; columna < juego.Tablero.Columnas; columna++)
                {
                    box = new PictureBox();
                    box.Size = size;
                    box.Location = new Point(fila*a, columna*a);
                    box.MouseClick += Box_MouseClick;

                    if (juego.Tablero.Celda(fila, columna).Color == Negocio.NColor.Blanco)
                    {

                        box.Enabled = false;
                        
                        box.BackColor = Color.White;
                    }
                    else
                    {
                        box.BackColor = Color.Black;
                    }
                    this.Controls.Add(box);
                }
            }
        }


        private void Box_MouseClick(object sender, MouseEventArgs e)
        {
            boxactual = (PictureBox)sender;
            boxactual.BackColor = Color.Yellow;
            ubiactual.Columna = boxactual.Location.X / a;
            ubiactual.Fila = boxactual.Location.Y / a;
            if (juego.Tablero.Verifico_Habilitada(ubiactual) ==true)
            {
                if (juego.Tablero.Verifico_Posicion(ubiactual).Ocupado != null)
                {
                    seleccionado = juego.Tablero.Verifico_Posicion(ubiactual);
                    boxsel = boxactual;
                }
                else
                {
                    if (seleccionado.Ocupado.Turno==true)
                    {
                        juego.Partida.Asignar_Turno(seleccionado.Ocupado);
                        a_mover = juego.Tablero.Verifico_Posicion(ubiactual);
                        if (!(juego.Partida is null))
                        {
                            if (Math.Abs(seleccionado.Ubicacion.Fila - a_mover.Ubicacion.Fila) == 1 && Math.Abs(seleccionado.Ubicacion.Columna - a_mover.Ubicacion.Columna) == 1)
                            {
                                juego.Tablero.Mover_Ficha_En_Tablero(seleccionado, a_mover);
                                if (a_mover.pieza_en_casilla.Tipo_Pieza == Tipo_Pieza.Reina)
                                {

                                    boxamov = boxactual;
                                    if (a_mover.pieza_en_casilla.Color == NColor.Azul)
                                    {
                                        boxamov.Image = azul_reina;
                                    }
                                    else
                                    {
                                        boxamov.Image = roja_reina;
                                    }

                                }
                                else
                                {
                                    boxamov = boxactual;
                                    boxamov.Image = boxsel.Image;
                                }

                                boxsel.Image = null;
                            }
                            else if (Math.Abs(seleccionado.Ubicacion.Fila - a_mover.Ubicacion.Fila) == 2 && Math.Abs(seleccionado.Ubicacion.Columna - a_mover.Ubicacion.Columna) == 2)
                            {
                                Casilla atrap = juego.Tablero.Come_Ficha_En_Tablero(seleccionado, a_mover);

                                PictureBox atrapado = null;
                                foreach (Control ctrl in Controls)
                                {
                                    if (ctrl is PictureBox)
                                    {
                                        int f = (ctrl.Location.Y / 50);
                                        int c = (ctrl.Location.X / 50);
                                        if (f == atrap.Ubicacion.Fila && c == atrap.Ubicacion.Columna)
                                        {
                                            atrapado = (PictureBox)ctrl;
                                        }
                                    }
                                }
                                if (a_mover.pieza_en_casilla.Tipo_Pieza == Tipo_Pieza.Reina)
                                {

                                    boxamov = boxactual;
                                    if (a_mover.pieza_en_casilla.Color == NColor.Azul)
                                    {
                                        boxamov.Image = azul_reina;
                                    }
                                    else
                                    {
                                        boxamov.Image = roja_reina;
                                    }

                                }

                                boxamov = boxactual;
                                boxamov.Image = boxsel.Image;
                                atrapado.Image = null;
                                boxsel.Image = null;
                            }
                        }
                    }
                }

                if (boxanterior == null)
                {
                    boxanterior = boxactual;
                }
                else if (boxanterior != boxactual)
                {
                    boxanterior.BackColor = Color.Black;
                    boxanterior = boxactual;
                }
            }

        }

        internal void Cargar_Fichas_Jugador(List<Jugador> jugadores)
        {
            this.Controls.Clear();
            this.CargaTablero();
            foreach (Jugador jugador in jugadores)
            {
                if (jugadores.IndexOf(jugador) == 0)
                {
                    Label label = new Label();
                    Button button = new Button();
                    button.Text = "Log Out";
                    button.Name = jugador.Nombre;
                    label.Visible = true;
                    label.Text = jugador.Nombre;
                    if (jugador.Bando==NColor.Azul)
                    {
                        label.ForeColor = Color.Blue;
                    }
                    label.Font = new Font("Courrie", 15, FontStyle.Regular);
                    Point lugar = new Point(450, 50);
                    Point point1 = new Point(450,100);
                    button.Location = point1;
                    label.Location = lugar;
                    this.Controls.Add(label);
                    this.Controls.Add(button);
                    this.Enabled = true;
                }
                else
                {
                    Label label = new Label();
                    Button button = new Button();
                    button.Text = "Log Out";
                    button.Name = jugador.Nombre;
                    button.Click += Button_Click;
                    label.Visible = true;
                    label.Text = jugador.Nombre;
                    if (jugador.Bando != NColor.Azul)
                    {
                        label.ForeColor = Color.Red;
                    }
                    label.Font = new Font("Courrie", 15, FontStyle.Regular);
                    Point lugar = new Point(450, 400);
                    Point point1 = new Point(450, 450);
                    button.Location = point1;
                    button.Click += Button_Click;
                    label.Location = lugar;
                    this.Controls.Add(label);
                    this.Controls.Add(button);
                }
                foreach (Pieza item in jugador.Piezas)
                {
                    box = (from picture in Controls.OfType<PictureBox>() where picture is PictureBox && picture.Location.X == item.Ubicacion.Columna * a && picture.Location.Y == item.Ubicacion.Fila * a select picture).SingleOrDefault();
                    if (item.Color==NColor.Azul)
                    {
                        box.Image = azul;
                    }
                    else
                    {
                        box.Image = roja;
                    }
                }
            }

            this.Refresh();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            juego.Logout(((Button)sender).Name);
        }

        private void Frmtablero_Click(object sender, EventArgs e)
        {
            if (juego.Partida is null)
            {
                this.Enabled = false;
            }
        }
    }
}
