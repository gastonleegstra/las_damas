﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
using Servicios;

namespace Damas
{
    public partial class Frm_registro_jugador : Form
    {
        public Juego juego;
        public Frm_registro_jugador()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            juego.Registrar_Usuario(Sesion.Encriptar(textBox1.Text), Sesion.Encriptar(textBox2.Text), Sesion.Encriptar(textBox3.Text));

            
            Close();
        }
    }
}
