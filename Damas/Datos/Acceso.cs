﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Datos
{
    public class Acceso:IDbParameter
    {
            private SqlConnection connection;

            public void Abrir() //Abre conexión con la Base de Datos
            {
                connection = new SqlConnection("Initial Catalog=LAS_DAMAS; Data Source=.; Integrated Security=SSPI");
                connection.Open();
            }

            public void Cerrar() //Cierra conexión con la Base de Datos
            {
                connection.Close();
                connection = null;
                GC.Collect();
            }
            public SqlParameter CrearParametro(string nombre, string valor)
            {
                SqlParameter p = new SqlParameter(nombre, valor);
                p.DbType = DbType.String;
                return p;
            }

            public SqlParameter CrearParametro(string nombre, float valor)
            {
                SqlParameter p = new SqlParameter(nombre, valor);
                p.DbType = DbType.Double;
                return p;
            }


            public SqlParameter CrearParametro(string nombre, bool valor)
            {
                SqlParameter p = new SqlParameter(nombre, valor);
                p.DbType = DbType.Boolean;
                return p;
            }

            public SqlParameter CrearParametro(string nombre, int valor)
            {
                SqlParameter p = new SqlParameter(nombre, valor);
                p.DbType = DbType.Int32;
                return p;
            }


            public SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text) //Crea sentencia sql o llama a proceso almacenado
            {
                SqlCommand comando = new SqlCommand(sql);
                comando.CommandType = tipo;
                if (parametros != null && parametros.Count > 0)
                {
                    comando.Parameters.AddRange(parametros.ToArray());
                }
                comando.Connection = connection;
                return comando;
            }

            public int Escribir(string sql, List<SqlParameter> parameters) //Realiza la escritura en la Base de Datos
            {
                int filasAfectadas = 0;
                SqlCommand comando = CrearComando(sql, parameters, CommandType.StoredProcedure);
                try
                {
                    filasAfectadas = comando.ExecuteNonQuery();
                }
                catch
                {

                    filasAfectadas = -1;
                }
                return filasAfectadas;
            }

            public DataTable Leer(string sql, List<SqlParameter> parameters) //Realizo la Lectura de Datos
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = CrearComando(sql, parameters, CommandType.StoredProcedure);

                DataTable table = new DataTable();

                adapter.Fill(table);

                return table;
            }
    }
}
