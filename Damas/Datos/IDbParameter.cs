﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Datos
{
    public interface IDbParameter
    {
        SqlParameter CrearParametro(string nombre, int valor);
    }
}
