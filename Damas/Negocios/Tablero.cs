﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace Negocio
{
    public class Tablero
    {
        private const int filas = 9;
        private const int columnas = 9;
        private List<Casilla> habilitadas = new List<Casilla>();
        private bool bando = false;
        private Casilla[,] plano = new Casilla[filas, columnas];

        public Tablero() 
        {
            int pos;
            Casilla nva_casilla;

            for (int fila = 1; fila < filas; fila++)
            {
                for (int columna = 1; columna < columnas; columna++)
                {
                    nva_casilla = new Casilla (fila,columna);
                    pos = fila + columna;
                    if (pos % 2 == 0)
                    {
                        nva_casilla.Color = NColor.Blanco;
                        nva_casilla.Habilitada = false;
                    }
                    else
                    {
                        nva_casilla.Color = NColor.Negro;
                        nva_casilla.Habilitada = true;
                        habilitadas.Add(nva_casilla);
                    }
                    plano[fila, columna] = nva_casilla;
                }
            }
        }

        public void Fichas_a_Tablero(List<Jugador> jugadores)
        {
            foreach (Jugador jugador in jugadores)
            {
                if (jugadores.IndexOf(jugador)==0)
                {
                    if (bando == false)
                    {
                        for (int i = 0; i < jugador.Piezas.Count; i++)
                        {
                            if (habilitadas[i].Ocupado == null)
                            {
                                jugador.Piezas[i].Ubicacion = habilitadas[i].Ubicacion;
                                habilitadas[i].pieza_en_casilla = jugador.Piezas[i];
                                habilitadas[i].Ocupado = jugador;
                            }
                        }
                        bando = true;
                    }
                }
                else
                {
                    int z = habilitadas.Count - 1;
                    for (int i = 0; i < jugador.Piezas.Count; i++)
                    {
                        if (habilitadas[z].Ocupado == null)
                        {
                            jugador.Piezas[i].Ubicacion = habilitadas[z].Ubicacion;
                            habilitadas[z].pieza_en_casilla = jugador.Piezas[i];
                            habilitadas[z].Ocupado = jugador;
                        }
                        z--;
                    }
                }
            }

        }

        public bool Verifico_Habilitada(Localizacion ubicacion)
        {
            bool ok;
            int acierto = (from c in habilitadas where c.Ubicacion.Columna == ubicacion.Columna && c.Ubicacion.Fila == ubicacion.Fila && c.Habilitada == true select c).Count();
            ok = acierto == 1 ? true : false;
            return ok;
        }

        public Casilla Verifico_Posicion(Localizacion ubicacion)
        {
            Casilla casilla;
            return  casilla = (from c in habilitadas where c.Ubicacion.Columna == ubicacion.Columna && c.Ubicacion.Fila == ubicacion.Fila select c).FirstOrDefault();
        }

        public void Mover_Ficha_En_Tablero(Casilla anterior, Casilla actual)
        {
            if (anterior.pieza_en_casilla.Tipo_Pieza == Tipo_Pieza.Normal)
            {
                if (Math.Abs(anterior.Ubicacion.Fila - actual.Ubicacion.Fila) == 1 && Math.Abs(anterior.Ubicacion.Columna - actual.Ubicacion.Columna) == 1)
                {
                    actual.Ocupado = anterior.Ocupado;
                    actual.pieza_en_casilla = anterior.pieza_en_casilla;
                    anterior.Ocupado = null;
                    anterior.pieza_en_casilla = null;

                    if (actual.Ubicacion.Fila==1 || actual.Ubicacion.Fila == 8)
                    {
                        actual.pieza_en_casilla.Tipo_Pieza = Tipo_Pieza.Reina;
                    }
                }
            }
            else 
            { 
                            
            }
        }

        public Casilla Come_Ficha_En_Tablero(Casilla anterior, Casilla actual)
        {
            Casilla atrapada = (from a in habilitadas
                                where (Math.Abs(anterior.Ubicacion.Fila - a.Ubicacion.Fila) == 1 &&
                                      Math.Abs(anterior.Ubicacion.Columna - a.Ubicacion.Columna) == 1 &&
                                      Math.Abs(actual.Ubicacion.Fila - a.Ubicacion.Fila) == 1 &&
                                      Math.Abs(actual.Ubicacion.Columna - a.Ubicacion.Columna) == 1)
                                select a).FirstOrDefault();
            if (atrapada.Ocupado.Bando!=anterior.Ocupado.Bando)
            {
                atrapada.Ocupado.Atrapadas(atrapada.pieza_en_casilla);
                atrapada.Ocupado = null;
                atrapada.pieza_en_casilla = null;
                actual.Ocupado = anterior.Ocupado;
                actual.pieza_en_casilla = anterior.pieza_en_casilla;
                anterior.Ocupado = null;
                anterior.pieza_en_casilla = null;

                if (actual.Ubicacion.Fila == 1 || actual.Ubicacion.Fila == 8)
                {
                    actual.pieza_en_casilla.Tipo_Pieza = Tipo_Pieza.Reina;
                }
            }
            return atrapada;
        }

        public int Filas 
        {
            get { return filas; }
        }

        public Casilla Celda(int fila, int columna)
        {
            return (Casilla)plano[fila, columna];
        }

        public int Columnas
        {
            get { return columnas; }
        }
        public Casilla[,] Plano 
        {
            get { return plano; }
        } 
    }
}