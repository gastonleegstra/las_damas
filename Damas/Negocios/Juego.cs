﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using Datos;
using Servicios;

namespace Negocio
{
    public class Juego
    {
        private Partida partida;
        private User user;
        private Tablero tablero = new Tablero();
        private List<User> usuarios = new List<User>();
        Acceso acceso = new Acceso();

        public Partida Partida 
        {
            get { return partida;}
        }
        public Juego() 
        {
            Carga_Usuarios();
        }

        public int Login(string text1, string text2)
        {
            int login;
            try
            {
                user  = (from usr in usuarios where usr.Usuario == text1 && usr.Password == text2 select usr).FirstOrDefault();
                login = (from usr in usuarios where usr.Usuario == text1 && usr.Password == text2 select usr).Count();
                Ingreso_Aceptado(login);   
            }
            catch 
            {

                login = -1;
            }
            return login;
        }

        private void Ingreso_Aceptado(int acceso) 
        {
            if (acceso == 1)
            {
                if (partida==null)
                {
                    Abrir_Partida();
                }
                
                Agrega_Jugador_A_Partida(Crear_Jugador(user));
            }
        }

        private Jugador Crear_Jugador(User user)
        {
            Jugador jugador = new Jugador();
            jugador.Nombre = Sesion.Desencriptar(user.Nombre);
            return jugador;
        }

        private void Abrir_Partida() 
        {
             partida = new Partida();
        }

        private void Agrega_Jugador_A_Partida(Jugador jugador) 
        {
                partida.Agregar_Jugador_Al_Juego(jugador);
        }

        public void Registrar_Usuario(string v1, string v2, string v3)
        {
            User user = new User(v1, v2, v3);
            Carga_Usuarios();
        }
        private List<User> Carga_Usuarios()
        {
            DataTable table = new DataTable();
            usuarios.Clear(); ;
            acceso.Abrir();
            table = acceso.Leer("Leer_Usuarios", null);
            acceso.Cerrar();
            foreach (DataRow row in table.Rows)
            {
                User user = new User();
                user.Nombre = row[0].ToString();
                user.Usuario = row[1].ToString();
                user.Password = row[2].ToString();

                usuarios.Add(user);
            }
            return usuarios;
        }
        public Tablero Tablero 
        {
            get { return tablero; }
        }

        public void Logout(string name)
        {
            Jugador logaout = partida.Jugadores.Where(j => j.Nombre == name).FirstOrDefault();
            partida.Jugadores.Remove(logaout);
            foreach (Jugador item in partida.Jugadores)
            {
                item.Turno = false;
            }
        }
    }
}