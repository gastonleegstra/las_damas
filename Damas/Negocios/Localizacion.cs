﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
    public class Localizacion
    {
        private int fila;

        public int Fila
        {
            get { return fila; }
            set { fila = value; }
        }

        private int columna;

        public int Columna
        {
            get { return columna; }
            set { columna = value; }
        }

        public Localizacion()
        {

        }
        public Localizacion(int x, int y) 
        {
            fila = x;
            columna = y;
        }
    }
}