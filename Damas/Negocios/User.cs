﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Datos;

namespace Negocio
{
    public class User
    {
        private string nombre;
        private Acceso acceso = new Acceso();
        private List<SqlParameter> parameters = new List<SqlParameter>();
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public User(string usr, string nom, string pass) 
        {
            Usuario = usr;
            Nombre = nom;
            Password= pass;
            Registrar();
        }
        public User() { }

        private void Registrar()
        {
            parameters.Clear();

            parameters.Add(acceso.CrearParametro("@Usuario", Usuario));
            parameters.Add(acceso.CrearParametro("@Nombre", Nombre));
            parameters.Add(acceso.CrearParametro("@Password", Password));
            acceso.Abrir();
            acceso.Escribir("Registrar_Usuario",parameters);
            acceso.Cerrar();
        }
    }
}