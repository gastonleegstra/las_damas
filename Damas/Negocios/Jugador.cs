﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
    public class Jugador
    {
        private string nombre;
        private NColor bando;
        private  const int maxficha = 12;

        private  List<Pieza>piezas = new List<Pieza>();
        private List<Pieza> atrapadas = new List<Pieza>();

        private bool turno;

        public bool Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        public string Nombre 
        {
            set { nombre = value; }
            get { return nombre; }
        }
        public  List<Pieza> Piezas 
        {
            get { return piezas; }
        }

        public NColor Bando 
        {
            get { return bando; }
            set { bando = value; }
        }

        public void Fichas()
        {
            for (int i = 0; i < maxficha; i++)
            {
                Pieza nvapieza = new Pieza();
                nvapieza.Color = bando;
                Piezas.Add(nvapieza);
            }
        }
        public void Atrapadas(Pieza pieza) 
        {
            atrapadas.Add(pieza);
            piezas.Remove(pieza);
        }
    }
}