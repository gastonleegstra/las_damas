﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Linq;

namespace Negocio
{
    public class Partida
    {
        Estado estado;
        List<Jugador> jugadores;
        private bool bando_azul = false;
        private bool bando_rojo = false;
        public Partida() 
        {
            jugadores = new List<Jugador>();
            estado = Estado.Inicio;
        }

        internal void Agregar_Jugador_Al_Juego(Jugador jugador)
        {
            if (bando_azul==false)
            {
                jugador.Bando = NColor.Azul;
                jugador.Fichas();
                bando_azul = true;
                jugador.Turno = false;
            }
            else
            {
                jugador.Bando = NColor.Rojo;
                jugador.Fichas();
                bando_rojo = true;
                jugador.Turno = true;
            }
            jugadores.Add(jugador);
        }
        
        public List<Jugador> Jugadores 
        {
            get { return jugadores; }
        }

        public void Asignar_Turno(Jugador jugador)
        {
            Jugador jugador1 = (from j in jugadores where j.Nombre != jugador.Nombre select j).FirstOrDefault();

            jugador.Turno = false;
            jugador1.Turno = true;
        }
    }
}