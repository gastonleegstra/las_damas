﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
    public class Pieza
    {
        NColor color;
        Tipo_Pieza tipo;
        Localizacion posicion;
        
        public NColor Color 
        {
            get { return color; }
            set { color = value; }
        }

        public Tipo_Pieza Tipo_Pieza 
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public Pieza() 
        {
            posicion = new Localizacion();
        }

        public Localizacion Ubicacion 
        {
            set { posicion = value; }
            get { return posicion; }
        }
    }
}