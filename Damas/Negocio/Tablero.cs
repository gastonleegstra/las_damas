﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio
{
    public class Tablero
    {
        private const int filas = 9;
        private const int columnas = 9;
        private List<Casilla> habilitadas = new List<Casilla>();
        private bool bando = false;
        private Casilla[,] plano = new Casilla[filas, columnas];

        public Tablero() 
        {
            int pos;
            Casilla nva_casilla;

            for (int fila = 1; fila < filas; fila++)
            {
                for (int columna = 1; columna < columnas; columna++)
                {
                    nva_casilla = new Casilla (fila,columna);
                    pos = fila + columna;
                    if (pos % 2 == 0)
                    {
                        nva_casilla.Color = NColor.Blanco;
                        nva_casilla.Habilitada = false;
                    }
                    else
                    {
                        nva_casilla.Color = NColor.Negro;
                        nva_casilla.Habilitada = true;
                        habilitadas.Add(nva_casilla);
                    }
                    plano[fila, columna] = nva_casilla;
                }
            }
        }

        public void Fichas_a_Tablero(Jugador jugador)
        {
            if (bando == false)
            {
                for (int i = 0; i < jugador.Piezas.Count; i++)
                {
                    if (habilitadas[i].Ocupado == false)
                    {
                        jugador.Piezas[i].Ubicacion = habilitadas[i].Ubicacion;
                        habilitadas[i].pieza_en_casilla = jugador.Piezas[i];
                    }
                }
                bando = true;
            }
            else
            {
                int z = habilitadas.Count - 1;
                for (int i = 0; i < jugador.Piezas.Count; i++)
                {
                    if (habilitadas[z].Ocupado == false)
                    {
                        jugador.Piezas[i].Ubicacion = habilitadas[z].Ubicacion;
                        habilitadas[z].pieza_en_casilla = jugador.Piezas[i];
                    }
                    z--;
                }
            }
        }

        public int Filas 
        {
            get { return filas; }
        }

        public Casilla Celda(int fila, int columna)
        {
            return (Casilla)plano[fila, columna];
        }

        public int Columnas
        {
            get { return columnas; }
        }
        public Casilla[,] Plano 
        {
            get { return plano; }
        } 
    }
}