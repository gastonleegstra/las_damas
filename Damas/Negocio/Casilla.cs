﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
    public class Casilla
    {
        private NColor color;
        private bool habilitada;
        Localizacion ubicacion = new Localizacion();
        private Pieza pieza;

        public Casilla(int fila, int columna) 
        {
            ubicacion.Fila = fila;
            ubicacion.Columna = columna;
        }
        public NColor Color
        {
            get { return color; }
            set { color = value; }
        }

        private bool ocupado;

        public bool Ocupado
        {
            get { return ocupado; }
            set { ocupado = value; }
        }

        public bool Habilitada 
        {
            get { return habilitada; }
            set { habilitada = value;} 
        }

        public Pieza pieza_en_casilla
        {
            get { return pieza; }
            set { pieza = value;}
        }

        public Localizacion Ubicacion 
        {
            get { return ubicacion; }
        }

    }
}