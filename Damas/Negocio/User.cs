﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Datos;

namespace Negocio
{
    public class User
    {
        private string nombre;
        private Acceso acceso = new Acceso();
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public User(string usr, string nom, string pass) 
        {
            Usuario = usr;
            Nombre = nom;
            Password= pass;
        }

        private void Registrar()
        {
            acceso.Abrir();
            acceso.Cerrar();
        }
    }
}